import React, {Component} from "react";
import {Platform} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import HomeScreen from "./src/screens/home";

const AppNavigator = createStackNavigator(
  {
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      title: "Home",
     // headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerTitleStyle: {
       // backgroundColor: '#f4511e',
        fontFamily: Platform.OS == "ios" ? "Helvetica" : ""
      }
    }
  }
},{
  initialRouteName:"Home"
}
);

 export default createAppContainer(AppNavigator);


