
export const fetchMenuItems = () => new Promise((resolve, reject) => {
    fetch('http://10.9.9.84:8080/api/menucategoryitems')
        .then(
            function(response) {
                if (response.status !== 200) {
                    console.log('Looks like there was a problem. Status Code: ' +
                        response.status);
                    reject(new Error('Something went Wrong'));
                }

                // Examine the text in the response
                response.json().then(function(data) {
                    resolve(data);
                });
            }
        )
        .catch(function(err) {
            reject(err);
        });
});