import React, { Component } from "react";

import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  Animated
} from "react-native"; //Step 1

class Panel extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    this.icons = {
      //Step 2
      up: require("../images/todown.png"),
      down: require("../images/totop.png")
    };

    this.state = {
      //Step 3
      title: props.title,
      expanded: false,
      animation: new Animated.Value()
    };
  }

  toggle() {
    this.props.sendFunction(!this.state.expanded);
     this.setState({
        expanded: !this.state.expanded //Step 2 
      });
    //Step 1
    let initialValue = this.state.expanded
        ? this.state.maxHeight + this.state.minHeight
        : this.state.minHeight,
      finalValue = this.state.expanded
        ? this.state.minHeight + 5
        : this.state.maxHeight + this.state.minHeight + 10;
   // console.log(initialValue, finalValue);

    this.setState({
        expanded: !this.state.expanded //Step 2 
      });

    this.state.animation.setValue(initialValue); //Step 3
    Animated.spring(
      //Step 4
      this.state.animation,
      {
        toValue: finalValue
      }
    ).start(); //Step 5
  }
  //set MAx height
  _setMaxHeight(event) {
    this.setState({
      maxHeight: event.nativeEvent.layout.height
    });
  }
//set MIN Height
  _setMinHeight(event) {
    this.setState({
      minHeight: event.nativeEvent.layout.height
    });
  }


  render() {
    let  icon = this.icons["up"];

    if (this.state.expanded) {
       icon = this.icons["down"]; //Step 4
    }

    //Step 5
    return (
      <Animated.View
        style={[styles.container, { height: this.state.animation }]}
      >
      <TouchableHighlight
            style={styles.button}
            onPress={this.toggle.bind(this)}
            underlayColor="#f1f1f1"
          >
        <View
          style={styles.titleContainer}
          onLayout={this._setMinHeight.bind(this)}
        >
          <Text style={styles.title}>{this.state.title}</Text>
          
            <Image style={styles.buttonImage} source={icon} />
         
        </View>
         </TouchableHighlight>
        {
          this.state.expanded && 
            <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
                  {this.props.children}
                </View>
        }
      
      </Animated.View>
    );
  }
}
export default Panel;

var styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    //margin: 10,
    overflow: "hidden"
  },
  titleContainer: {
    flexDirection: "row"
  },
  title: {
    flex: 1,
    //padding: 10,
    paddingRight: 10,
    paddingTop: 5,
    color: "#2a2f43"
    //fontWeight: "bold"
  },
  button: {},
  buttonImage: {
    width: 30,
    height: 25
  },
  body: {
    padding: 10,
    paddingTop: 0
  }
});
