import React, {Component} from "react";
import {fetchMenuItems} from "../config/menuService";
import {Dimensions, FlatList, Image, StyleSheet, Text, View, ActivityIndicator} from "react-native";
import Panel from "../components/panel";
import {List, ListItem} from "native-base";

const {width, height} = Dimensions.get("window");

class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            expanded: false
        };

    }

    static navigationOptions = ({navigation}) => {
        console.log(navigation);
        return {
            // title: navigation.getParam('otherParam', 'A Nested Details Screen'),
        };
    };

    // onpress for list item
    onItemPress = (Obj, index) => {
        console.log(Obj);

        return <View/>;
    };

    componentDidMount() {
        fetchMenuItems()
            .then(data => {
                this.setState({ data : data});
            })
            .catch(err => console.log(err))
    };

    //get item from list using reduce
    getMenuString = arr => {
        return arr.reduce((initialValue, currValue, index) => {
            if (index > 1) {
                return initialValue + "..";
            }
            return initialValue + currValue.menuItemName + "," + " ";
        }, arr.length + " items:");
    };

    myTest = (e) => {
        console.log(e);
        this.setState({
            expanded: e
        })
    };

    _renderItem = (item, type) => {

        // const itemsLength = Object.keys(item.item.Items).length;
        // console.log(itemsLength);
        // console.log(item.item.Items);
        // console.log(type);

        return (
            <View>
                <List>
                    <ListItem>
                        <View style={{flexDirection: "column", flex: 1}}>
                            <Text style={{fontSize: 16, fontWeight: "bold"}}>
                                {item.item.menuCategoryName}
                            </Text>
                            <Panel title={this.getMenuString(item.item.menuItems)} sendFunction={(e) => this.myTest(e)}>

                                <View>
                                    <FlatList
                                        data={item.item.menuItems}
                                        keyExtractor={(item, index) => item.key}
                                        renderItem={({item}) => (
                                            <View
                                                style={{
                                                    alignItems: "center",
                                                    flexDirection: "row",
                                                    justifyContent: "space-between",
                                                    alignContent: "space-between"
                                                }}
                                            >
                                                <View
                                                    style={{
                                                        flexDirection: "row",
                                                        marginTop: 15,
                                                        marginRight: 15,
                                                        marginBottom: 15
                                                    }}
                                                >
                                                    <View>
                                                        {type === "Veg" ? (
                                                            <Image
                                                                source={require("../images/Veg.png")}
                                                                style={{
                                                                    height: 10,
                                                                    width: 10,
                                                                    marginRight: 5,
                                                                    marginTop: 3,
                                                                    marginBottom: 5
                                                                }}
                                                            />
                                                        ) : (
                                                            <Image
                                                                source={require("../images/Non-Veg.png")}
                                                                style={{
                                                                    height: 10,
                                                                    width: 10,
                                                                    marginRight: 5,
                                                                    marginTop: 2,
                                                                    marginBottom: 5
                                                                }}
                                                            />
                                                        )}
                                                    </View>
                                                    <View style={{flexDirection: "column"}}>
                                                        <Text style={{fontSize: 12}}>{item.menuItemName}</Text>

                                                        <Text style={{fontSize: 12}}>₹ {item.menuItemPrice}</Text>
                                                    </View>
                                                </View>
                                                <View>
                                                    <Text
                                                        style={{
                                                            fontSize: 12,
                                                            borderWidth: 1,
                                                            borderColor: "green",
                                                            paddingLeft: 10,
                                                            paddingBottom: 5,
                                                            paddingTop: 5,
                                                            paddingRight: 10
                                                        }}
                                                    >
                                                        ADD
                                                    </Text>
                                                </View>
                                            </View>
                                        )}
                                    />
                                </View>
                            </Panel>
                        </View>
                    </ListItem>
                </List>
            </View>
        );
    };

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View>
                {this.state.data ? <FlatList
                    data={this.state.data}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={item => this._renderItem(item, item.item.menuCategoryItemType)}
                    ListEmptyComponent={
                        <View
                            style={{
                                height: height / 2.5,
                                flexDirection: "column",
                                alignItems: "center",
                                justifyContent: "flex-end"
                            }}
                        >
                            <Text style={{color: "#666666", fontSize: 16}}>
                                No Categories
                            </Text>
                        </View>
                    }
                /> : <ActivityIndicator size="large" color="#0000ff" />}
            </View>
        );
    }
}

export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#f4f7f9",
        paddingTop: 30
    }
});
